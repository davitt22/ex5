package com.example.ex5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.UserHandle
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }
    private fun init() {
        logInButton.setOnClickListener {
            login()
        }

        }
    private fun login(){
            val email: String = emailBox.text.toString()
            val password = passwordBox.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty()) {
                val intent = Intent(this , ProfileActivity1::class.java)
                startActivity(intent)

            }else{ Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show() }
    }



}




