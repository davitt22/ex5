package com.example.ex5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.Glide.init
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_profile1.*
import kotlinx.android.synthetic.main.activity_profile1.Image1

class ProfileActivity1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile1)
        init()
    }
    private fun init(){
        Glide.with(this)
            .load("https://149369349.v2.pressablecdn.com/wp-content/uploads/2012/10/twitter-cover.jpg")
            .placeholder(R.mipmap.ic_launcher).into(backgroundImage)
        Glide.with(this)
            .load("https://content.fortune.com/wp-content/uploads/2018/07/gettyimages-961697338.jpg")
            .placeholder(R.mipmap.ic_launcher).into(profileImage)
        Glide.with(this)
            .load("https://cdn.iconscout.com/icon/free/png-256/apple-mail-493152.png")
            .placeholder(R.mipmap.ic_launcher).into(mail)
        Glide.with(this)
            .load("https://i.pinimg.com/originals/79/db/d3/79dbd38bce56b49fdc7a04d3b01dee41.png")
            .placeholder(R.mipmap.ic_launcher).into(tel)
        Glide.with(this)
            .load("https://static.apester.com/user-images/b9/b9fc3882dd0681d7cd8024e7b90df7e3.gif")
            .placeholder(R.mipmap.ic_launcher).into(nationality)
        Glide.with(this)
            .load("https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5c76b7d331358e35dd2773a9%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D0%26cropX2%3D4401%26cropY1%3D0%26cropY2%3D4401")
            .placeholder(R.mipmap.ic_launcher).into(Image1)
        Glide.with(this)
            .load("https://static01.nyt.com/images/2020/11/17/business/17techhearing-facebookpreview/merlin_163192374_92604511-ae28-43ba-8d7f-d3fbdae53e01-mobileMasterAt3x.jpg")
            .placeholder(R.mipmap.ic_launcher).into(Image2)
        Glide.with(this)
            .load("https://image.cnbcfm.com/api/v1/image/105879772-1556550327424gettyimages-1057466684.jpeg?v=1591972364")
            .placeholder(R.mipmap.ic_launcher).into(Image3)




    }
}